﻿using Forum.Data.Models;
using System;

namespace Forum.Service.DTOs
{
    public class PostDTO
    {
        public PostDTO()
        {

        }
        public PostDTO(Post post)
        {
            Id = post.Id;
            CreatedOn = post.CreatedOn;
            PossitiveVoteCount = GetPossitiveVotes(post);
            NegativeVoteCount = GetNegativeVotes(post);
            CommentCount = post.Comments.Count;
            AuthorName = post.Author.IsDeleted ? "[Deleted user]" : post.Author.DisplayName;
            Title = post.Title;
            Content = post.Content;
            CategoryName = post.Category.Title;
        }
        public int Id { get; set; }
        public string AuthorName { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string CategoryName { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int PossitiveVoteCount { get; set; }
        public int NegativeVoteCount { get; set; }
        public int CommentCount { get; set; }

        private int GetPossitiveVotes(Post post)
        {
            var votes = 0;
            foreach (var vote in post.Votes)
            {
                if (vote.VoteType == Data.Enums.VoteType.Upvote && vote.IsDeleted == false) votes++;
            }
            return votes;
        }

        private int GetNegativeVotes(Post post)
        {
            var votes = 0;
            foreach (var vote in post.Votes)
            {
                if (vote.VoteType == Data.Enums.VoteType.Downvote && vote.IsDeleted == false) votes++;
            }
            return votes;
        }
    }
}
