﻿using Forum.Data;
using System.ComponentModel.DataAnnotations;

namespace Forum.WebAPI.Models.WebModels
{
    public class UserWebModel
    {
        [Required]
        [StringLength(Constants.NAME_MAX_LEN, MinimumLength = Constants.NAME_MIN_LEN, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Username { get; set; }
        [Required]
        [StringLength(50, MinimumLength = Constants.REQ_PASSWORD_LEN, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Password { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(Constants.NAME_MAX_LEN, MinimumLength = Constants.NAME_MIN_LEN, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string DisplayName { get; set; }
    }
}
