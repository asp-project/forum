﻿using Forum.Data.Abstraction;
using Forum.Data.Enums;

namespace Forum.Data.Models
{
    public class PostVote : Entity
    {
        public VoteType VoteType { get; set; }
        public int PostId { get; set; }
        public virtual Post Post { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
