﻿using Forum.Data.Enums;
using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Service.Contracts;
using Forum.Service.Exceptions;
using System.Linq;

namespace Forum.Service.Services
{
    public class VoteService : IVoteService
    {
        private readonly IPostVoteRepository postVoteRepository;
        private readonly IPostRepository postRepository;
        private readonly IUserRepository userRepository;
        private readonly ICommentRepository commentRepository;
        private readonly ICommentVoteRepository commentVoteRepository;

        public VoteService(IPostVoteRepository postVoteRepository, IPostRepository postRepository,
            ICommentRepository commentRepository, ICommentVoteRepository commentVoteRepository,
            IUserRepository userRepository)
        {
            this.postRepository = postRepository;
            this.postVoteRepository = postVoteRepository;
            this.userRepository = userRepository;
            this.commentRepository = commentRepository;
            this.commentVoteRepository = commentVoteRepository;
        }

        public VoteType GetUserVoteTypeForCommnet(int commentId, int userId)
        {
            var vote = this.commentVoteRepository.QueryAll()
                        .FirstOrDefault(x => x.UserId == userId && x.CommentId == commentId);

            return vote != null ? vote.VoteType : VoteType.None;
        }

        public VoteType GetUserVoteTypeForPost(int postId, int userId)
        {
            var vote = this.postVoteRepository.QueryAll()
                .FirstOrDefault(x => x.UserId == userId && x.PostId == postId);

            return vote != null ? vote.VoteType : VoteType.None;
        }

        public void ToggleCommentVote(int commentId, int userId, VoteType newVoteType)
        {
            var comment = this.commentRepository.QueryAll()
                .FirstOrDefault(x => x.Id == commentId);


            var user = this.userRepository.QueryAll().
                        FirstOrDefault(user => user.Id == userId);

            if (comment == null || user == null)
            {
                throw new EntityNotFoundException("Something went wrong. Non-existing comment or user");
            }

            var prevUserVote = this.commentVoteRepository.QueryAll()
                .FirstOrDefault(x => x.CommentId == commentId && x.UserId == userId);

            var newVote = new CommentVote
            {
                VoteType = newVoteType,
                UserId = userId,
                CommentId = commentId,
            };

            if (prevUserVote == null)
            {
                this.commentVoteRepository.Add(newVote);
            }
            else
            {
                this.commentVoteRepository.Remove(prevUserVote);

                if (prevUserVote.VoteType != newVote.VoteType)
                {
                    this.commentVoteRepository.Add(newVote);
                }
            }
        }

        public void TogglePostVote(int postId, int userId, VoteType newVoteType)
        {
            var post = this.postRepository.QueryAll()
                .FirstOrDefault(x => x.Id == postId);


            var user = this.userRepository.QueryAll().
                        FirstOrDefault(user => user.Id == userId);

            if (post == null || user == null)
            {
                throw new EntityNotFoundException("Something went wrong. Non-existing post or user");
            }

            var prevUserVote = this.postVoteRepository.QueryAll()
                .FirstOrDefault(x => x.PostId == postId && x.UserId == userId);

            var newVote = new PostVote
            {
                VoteType = newVoteType,
                UserId = userId,
                PostId = postId,
            };

            if (prevUserVote == null)
            {
                this.postVoteRepository.Add(newVote);
            }
            else
            {
                this.postVoteRepository.Remove(prevUserVote);

                if (prevUserVote.VoteType != newVote.VoteType)
                {
                    this.postVoteRepository.Add(newVote);
                }
            }
        }
    }
}
