﻿using Forum.Data.Models;

namespace Forum.Repository.Contacts
{
    public interface ICommentVoteRepository : IRepository<CommentVote>
    {
    }
}
