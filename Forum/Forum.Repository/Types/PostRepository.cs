﻿using Forum.Data;
using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Extensions;
using Forum.Repository.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Forum.Repository.Types
{
    public class PostRepository : Repository<Post>, IPostRepository
    {
        public PostRepository(ForumDbContext context) : base(context)
        {
        }
        public override IQueryable<Post> QueryAll()
        {
            return base.QueryAll()
                        .IncludePostBasics()
                .AsSplitQuery();
        }
        public override IQueryable<Post> QueryAllWithDeleted()
        {
            return base.QueryAllWithDeleted()
                        .IncludePostBasics()
                .AsSplitQuery();
        }
        public IQueryable<Post> QueryAllWithOptions(Order order, Pagination pagination, PostFilter filter)
        {
            return base.QueryAll()

                    .IncludePostBasics()
                .AsSplitQuery()
                    .FilterPost(filter)
                    .AddSort(order)
                    .AddPagination(pagination);
        }

        public IQueryable<Post> QueryAllWithDeletedWithOptions(Order order, Pagination pagination, PostFilter filter)
        {
            return base.QueryAllWithDeleted()
                    .IncludePostBasics()
                .AsSplitQuery()
                    .FilterPost(filter)
                    .AddSort(order)
                    .AddPagination(pagination);
        }
    }
}
