# Wellcome to Share Ideas Project by Aleks & Anjelo DEC-2021
Online discussion site where users can hold conversations by posting, commenting and liking or disliking messages.
### Tech stack: 
- .NET Core, Entity Framework Core, Repository pattern, REST API, JWT, ASP.NET MVC, MSSQL, Swagger,  GitLab, HTML/CSS, JavaScript, jQuery.

![logo](img/logo.png)

### DB Table:

![diagram](img/diagram.png)
