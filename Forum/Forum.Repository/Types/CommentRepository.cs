﻿using Forum.Data;
using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Extensions;
using Forum.Repository.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Forum.Repository.Types
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(ForumDbContext context) : base(context)
        {

        }
        public override IQueryable<Comment> QueryAll()
        {
            return base.QueryAll()
                .IncludeCommentBasics()
                .AsSplitQuery();
        }
        public override IQueryable<Comment> QueryAllWithDeleted()
        {
            return base.QueryAllWithDeleted()
                .IncludeCommentBasics()
                .AsSplitQuery();
        }
        public IQueryable<Comment> QueryAll(Pagination pagination)
        {
            return base.QueryAll()
                .IncludeCommentBasics()
                .AddPagination(pagination)
                .AsSplitQuery();
        }

        public IQueryable<Comment> QueryAllWithDeleted(Pagination pagination)
        {
            return base.QueryAll()
                .IncludeCommentBasics()
                .AsSplitQuery()
                .AddPagination(pagination);
        }
    }
}
