﻿using Forum.Data.Models;
using Forum.MVC.Exceptions;
using Forum.Service.Contracts;

namespace Forum.MVC.Helpers
{
    public class AuthHelper
    {
        private static readonly string ErrorMessage = "Invalid authentication info.";

        private readonly IUserService userService;

        public AuthHelper(IUserService userService)
        {
            this.userService = userService;
        }


        public User TryGetUser(string username, string password)
        {
            var user = this.TryGetUser(username, password);
            if (user.Password != password)
            {
                throw new AuthenticationException(ErrorMessage);
            }
            return user;
        }
    }
}
