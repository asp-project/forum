﻿using Forum.Data.Abstraction;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Data.Models
{
    public class User : Entity
    {
        public int RoleId { get; set; } = 1;
        public virtual Role Role { get; set; }

        [Required, MinLength(Constants.NAME_MIN_LEN), MaxLength(Constants.NAME_MAX_LEN)]
        public string Username { get; set; }
        [Required, MaxLength(Constants.REQ_PASSWORD_LEN)]
        public string Password { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string ProfileUrl { get; set; }

        [Required, MinLength(Constants.NAME_MIN_LEN), MaxLength(Constants.NAME_MAX_LEN)]
        public string DisplayName { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? UpdatedOn { get; set; }
        public bool IsBlocked { get; set; }
        public virtual ICollection<PostVote> Votes { get; set; } = new List<PostVote>();

        public virtual ICollection<Post> Posts { get; set; } = new List<Post>();

        public virtual ICollection<Category> Categories { get; set; } = new List<Category>();

        public virtual ICollection<Comment> Comments { get; set; } = new List<Comment>();
    }
}
