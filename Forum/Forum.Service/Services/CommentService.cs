﻿using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Models;
using Forum.Service.Contracts;
using Forum.Service.DTOs;
using Forum.Service.Exceptions;
using System;
using System.Linq;

namespace Forum.Service.Services
{
    public class CommentService : ICommentService
    {
        private readonly IPostRepository postsRepository;
        private readonly ICommentRepository commentRepository;
        private readonly IUserRepository userRepository;

        public CommentService(
            ICommentRepository commentRepository,
            IPostRepository postsRepository, IUserRepository userRepository)
        {
            this.postsRepository = postsRepository;
            this.commentRepository = commentRepository;
            this.userRepository = userRepository;
        }

        public IQueryable<CommentDTO> GetAll(Pagination pagination, bool includeDeleted = false)
        {
            if (!includeDeleted)
            {
                var comments = this.commentRepository.QueryAll(pagination)
                    .Select(c => new CommentDTO(c));

                if (!comments.Any())
                {
                    throw new EntityNotFoundException("There is no such comment.");
                }
                return comments;
            }
            else
            {
                var comments = this.commentRepository.QueryAllWithDeleted(pagination)
                    .Select(c => new CommentDTO(c));

                if (!comments.Any())
                {
                    throw new EntityNotFoundException("There is no such comment.");
                }
                return comments;
            }
        }

        public IQueryable<CommentDTO> GetById(int id, bool includeDeleted = false)
        {
            if (!includeDeleted)
            {
                var comment = this.commentRepository.QueryAll()
                    .Where(x => x.Id == id)
                    .Select(c => new CommentDTO(c));

                if (!comment.Any())
                {
                    throw new EntityNotFoundException("There is no such comment.");
                }
                return comment;
            }
            else
            {
                var comment = this.commentRepository.QueryAllWithDeleted()
                    .Where(x => x.Id == id)
                    .Select(c => new CommentDTO(c));
                if (!comment.Any())
                {
                    throw new EntityNotFoundException("There is no such comment.");
                }
                return comment;
            }
        }

        public virtual void Delete(int id)
        {
            var entity = this.commentRepository.QueryAll()
                .Where(x => x.Id == id)
                .FirstOrDefault();

            if (entity == null)
            {
                throw new EntityNotFoundException("There is no such comment.");
            }

            this.commentRepository.Remove(entity);
        }

        public void Restore(int id)
        {
            var entity = this.commentRepository.QueryAllWithDeleted()
                .Where(x => x.Id == id)
                .FirstOrDefault();

            if (entity == null)
            {
                throw new EntityNotFoundException("There is no such comment.");
            }

            this.commentRepository.Restore(entity);
        }

        public void AddCommentToPost(Comment comment, int userId)
        {
            var user = this.userRepository.QueryAll().FirstOrDefault(user => user.Id == userId);
            if (user == null)
            {
                throw new EntityNotFoundException();
            }
            else if (user.IsBlocked)
            {
                throw new Exception("Blocked users cannot create posts!");
            }

            comment.AuthorId = userId;

            var post = this.postsRepository
                .QueryAll()
                .Where(x => x.Id == comment.PostId)
                .FirstOrDefault();

            if (post == null)
            {
                throw new EntityNotFoundException("There is no such post.");
            }

            this.commentRepository.Add(comment);
        }

        public void Update(int id, Comment entity, int userId)
        {
            var commentToUpdate = this.commentRepository.QueryAll().FirstOrDefault(c => c.Id == id);

            if (commentToUpdate == null)
            {
                throw new EntityNotFoundException("There is no such comment.");
            }

            if (commentToUpdate.AuthorId != userId)
            {
                throw new Exception("You can update only your comments");
            }

            commentToUpdate.Content = entity.Content;
            this.commentRepository.Update(commentToUpdate);
        }

        public void DeleteAsUser(int id, int userId)
        {
            var entity = this.commentRepository.QueryAll()
                .Where(x => x.Id == id)
                .FirstOrDefault();

            if (entity == null)
            {
                throw new EntityNotFoundException("There is no such comment.");
            }

            if (entity.AuthorId != userId)
            {
                throw new UnauthorizedAccessException("You can only delete your comments");
            }
            this.commentRepository.Remove(entity);
        }
    }
}
