﻿using Microsoft.AspNetCore.Mvc;

namespace Forum.WebAPI.Models
{
    public class PostFilterQuery
    {
        [FromQuery(Name = "author name")]
        public string AuthorName { get; set; }
        [FromQuery(Name = "title")]
        public string Title { get; set; }
        [FromQuery(Name = "content")]
        public string Content { get; set; }
    }
}
