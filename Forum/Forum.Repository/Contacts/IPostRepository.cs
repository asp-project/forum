﻿using Forum.Data.Models;
using Forum.Repository.Models;
using System.Linq;

namespace Forum.Repository.Contacts
{
    public interface IPostRepository : IRepository<Post>
    {
        public IQueryable<Post> QueryAllWithOptions(Order order = default, Pagination pagination = default, PostFilter filter = null);

        public IQueryable<Post> QueryAllWithDeletedWithOptions(Order order = default, Pagination pagination = default, PostFilter filter = null);
    }
}
