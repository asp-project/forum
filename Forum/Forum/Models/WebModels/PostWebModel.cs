﻿using Forum.Data;
using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.WebAPI.Models.WebModels
{
    public class PostWebModel
    {
        [Required]
        [StringLength(Constants.CATEGORY_TITLE_MAX_LEN, MinimumLength = 0, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "CategoryId must be positive")]
        public int CategoryId { get; set; }
    }
}
