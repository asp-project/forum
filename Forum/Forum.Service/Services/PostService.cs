﻿using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Models;
using Forum.Service.Contracts;
using Forum.Service.DTOs;
using Forum.Service.Exceptions;
using System;
using System.Linq;

namespace Forum.Service.Services
{
    public class PostService : IPostService
    {
        private readonly IPostRepository postRepository;
        private readonly ICommentRepository commentRepository;
        private readonly IUserRepository userRepository;
        public PostService(IPostRepository postRepository,
            ICommentRepository commentRepository, IUserRepository userRepository)
        {
            this.postRepository = postRepository;
            this.commentRepository = commentRepository;
            this.userRepository = userRepository;
        }
        public IQueryable<PostDTO> GetAll(Pagination pagination, Order order, PostFilter filter, bool includeDeleted = false)
        {
            if (!includeDeleted)
            {
                var posts = this.postRepository.QueryAllWithOptions(order, pagination, filter)
                    .Select(p => new PostDTO(p));

                if (!posts.Any())
                {
                    throw new EntityNotFoundException("There are no posts yet.");
                }
                return posts;
            }
            else
            {
                var posts = this.postRepository.QueryAllWithDeletedWithOptions(order, pagination, filter)
                    .Select(p => new PostDTO(p));

                if (!posts.Any())
                {
                    throw new EntityNotFoundException("There are no posts yet.");
                }
                return posts;
            }
        }

        public IQueryable<PostDTO> GetById(int id, bool includeDeleted = false)
        {
            if (!includeDeleted)
            {
                var post = this.postRepository.QueryAll()
                    .Where(x => x.Id == id)
                    .Select(p => new PostDTO(p));

                if (!post.Any())
                {
                    throw new EntityNotFoundException("There is no such post.");
                }
                return post;
            }
            else
            {
                var post = this.postRepository.QueryAllWithDeleted()
                    .Where(x => x.Id == id)
                    .Select(p => new PostDTO(p));

                if (!post.Any())
                {
                    throw new EntityNotFoundException("There is no such post.");
                }
                return post;
            }
        }

        public virtual void DeleteAsUser(int id, int userId)
        {
            var entity = this.postRepository.QueryAll()
                .Where(x => x.Id == id)
                .FirstOrDefault();

            if (entity == null)
            {
                throw new EntityNotFoundException("There is no such post.");
            }

            if (entity.AuthorId != userId)
            {
                throw new UnauthorizedAccessException("You can only detele your posts!");
            }

            foreach (var comments in entity.Comments)
            {
                this.commentRepository.Remove(comments);
            }

            this.postRepository.Remove(entity);
        }

        public void Restore(int id)
        {
            var entity = this.postRepository.QueryAllWithDeleted()
                .Where(x => x.Id == id)
                .FirstOrDefault();

            if (entity == null)
            {
                throw new EntityNotFoundException("There is no such post.");
            }

            this.postRepository.Restore(entity);
        }

        public void Create(Post post, int userId)
        {
            var user = this.userRepository.QueryAll().FirstOrDefault(user => user.Id == userId);

            if (user == null)
            {
                throw new EntityNotFoundException();
            }
            else if (user.IsBlocked)
            {
                throw new Exception("Blocked users cannot create posts!");
            }
            post.AuthorId = user.Id;
            this.postRepository.Add(post);
        }

        public IQueryable<PostDTO> GetRecent()
        {
            var posts = this.postRepository.QueryAll()
                .OrderByDescending(x => x.CreatedOn)
                .Take(10)
                .Select(p => new PostDTO(p));

            if (!posts.Any())
            {
                throw new EntityNotFoundException("There is no such post.");
            }
            return posts;
        }

        public IQueryable<PostDTO> GetTopRated()
        {
            var posts = this.postRepository.QueryAll()
                .OrderByDescending(x => x.VoteCount)
                .ThenByDescending(x => x.CreatedOn)
                .Take(10)
                .Select(p => new PostDTO(p));

            if (!posts.Any())
            {
                throw new EntityNotFoundException("There is no such post.");
            }
            return posts;
        }

        public void Update(int id, Post entity, int userId)
        {
            var postToUpdate = this.postRepository.QueryAll().FirstOrDefault(p => p.Id == id);

            if (postToUpdate == null)
            {
                throw new EntityNotFoundException("There is no such post.");
            }
            if (postToUpdate.AuthorId != userId)
            {
                throw new Exception("You can only update your posts!");
            }

            postToUpdate.Title = entity.Title;
            postToUpdate.Content = entity.Content;
            postToUpdate.UpdatedOn = DateTime.Now;
            this.postRepository.Update(postToUpdate);
        }

        public void Delete(int id)
        {
            var entity = this.postRepository.QueryAll()
                                    .Where(x => x.Id == id)
                                    .FirstOrDefault();

            if (entity == null)
            {
                throw new EntityNotFoundException("There is no such post.");
            }

            foreach (var comments in entity.Comments)
            {
                this.commentRepository.Remove(comments);
            }

            this.postRepository.Remove(entity);
        }
    }
}
