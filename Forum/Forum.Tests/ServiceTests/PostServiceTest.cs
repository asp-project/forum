﻿using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Models;
using Forum.Service.Exceptions;
using Forum.Service.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Tests.ServiceTests
{
    [TestClass]
    public class PostServiceTest
    {
        private readonly PostService sut;
        private List<Post> testList;

        private readonly Order order = new Order();
        private readonly PostFilter filter = new PostFilter();
        private readonly Pagination pagination = new Pagination(0, int.MaxValue);

        private readonly Mock<IPostRepository> postRepository = new Mock<IPostRepository>();
        private readonly Mock<ICommentRepository> commentRepository = new Mock<ICommentRepository>();
        private readonly Mock<IUserRepository> userRepository = new Mock<IUserRepository>();

        public PostServiceTest()
        {
            sut = new PostService(this.postRepository.Object, this.commentRepository.Object, this.userRepository.Object);
            testList = new List<Post>()
            {
                new Post
                {
                    Id = 2,
                    AuthorId = 1,
                    Author = new User()
                    {
                        Username = "Alexandro"
                    },
                    Category = new Category()
                    {
                        Id =1,
                    },
                    CreatedOn = DateTime.Now,
                    Content = "Test Content",
                    CategoryId= 1,
                    VoteCount = 1,
                    Title = "Test Title",

                },
                new Post
                {
                    Id = 4,
                    AuthorId = 1,
                    Author = new User()
                    {
                        Username = "DelPiero"
                    },
                    Category = new Category()
                    {
                        Id =1,
                    },
                    CreatedOn = DateTime.Now,
                    Content = "Test Content 2",
                    CategoryId= 1,
                    VoteCount = 1,
                    Title = "Test Title 2",
                }

            };

        }
        [Ignore]
        [TestMethod]
        public void PostShould_ReturnAllPosts()
        {
            //Arrange           

            postRepository.Setup(x => x.QueryAll()).Returns(testList.AsQueryable());

            //Act
            var result = sut.GetAll(this.pagination, this.order, this.filter);

            //Assert
            Assert.AreEqual(testList.Count, result.ToList().Count());

        }


        [TestMethod]
        public void CommentsShould_ThrowExceptionWhen_CommentsNotAdded()
        {
            //Arrange
            testList = new List<Post>();
            postRepository.Setup(x => x.QueryAll()).Returns(testList.AsQueryable());

            //Act & Assert
            var e = Assert.ThrowsException<EntityNotFoundException>(() => sut.GetAll(this.pagination, this.order, this.filter));
            Assert.AreSame(e.Message, "There are no posts yet.");

        }
        [Ignore]
        [TestMethod]
        public void CommentsShould_ReturnAllComments_IncludeDeleted()
        {
            //Arrange
            postRepository.Setup(x => x.QueryAll()).Returns(testList.AsQueryable());

            //Act

            var result = sut.GetAll(this.pagination, this.order, this.filter);

            //Assert
            Assert.AreEqual(testList.Count, result.ToList().Count());

        }

        [TestMethod]
        public void CommentsShould_ThrowExceptionWhen_CommentsNotAdded_IncludeDeleted()
        {
            //Arrange
            testList = new List<Post>();
            postRepository.Setup(x => x.QueryAll()).Returns(testList.AsQueryable());

            //Act & Assert
            var e = Assert.ThrowsException<EntityNotFoundException>(() => sut.GetAll(this.pagination, this.order, this.filter));
            Assert.AreSame(e.Message, "There are no posts yet.");

        }
    }
}
