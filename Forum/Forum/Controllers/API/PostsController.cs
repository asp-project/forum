﻿using Forum.Service.Contracts;
using Forum.WebAPI.Extension;
using Forum.WebAPI.Models;
using Forum.WebAPI.Models.ModelMapper;
using Forum.WebAPI.Models.WebModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Forum.WebAPI.Controllers.API
{
    //[Authorize]    

    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostService postService;
        public PostsController(IPostService postService)
        {
            this.postService = postService;
        }

        // GET: api/<PostsController>
        [HttpGet]
        public IActionResult GetAll([FromQuery] bool withDeleted,
            [FromQuery] PaginationModel paginationModel,
            [FromQuery] OrderModel orderModel,
            [FromQuery] PostFilterQuery query)
        {
            var pagination = ModelMapper.ToModel(paginationModel);
            var order = ModelMapper.ToModel(orderModel);
            var filter = ModelMapper.ToModel(query);
            var posts = postService.GetAll(pagination, order, filter, withDeleted);
            return Ok(posts);
        }

        // GET api/<PostsController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id, [FromQuery] bool withDeleted)
        {
            return Ok(postService.GetById(id, withDeleted));
        }

        // GET 
        [AllowAnonymous]
        [HttpGet("top")]
        public IActionResult GetTopPosts()
        {
            var posts = postService.GetTopRated();

            return Ok(posts);
        }

        // GET
        [AllowAnonymous]
        [HttpGet("recent")]
        public IActionResult Get()
        {
            var posts = postService.GetRecent();

            return Ok(posts);
        }

        [Authorize(Roles = "Moderator, Admin")]
        [HttpPost("create")]
        public IActionResult Create([FromBody] PostWebModel model)
        {

            var userId = 1;
            var post = ModelMapper.ToModel(model);

            postService.Create(post, userId);

            return Created("Sucessfull", model);
        }

        [HttpPost("update/{id}")]
        public IActionResult Update(int id, [FromBody] PostUpdateModel model)
        {
            var userId = User.GetUserID();
            var post = ModelMapper.ToModel(model);

            postService.Update(id, post, userId);
            return Ok("Post updated");
        }

        [Authorize(Roles = "Moderator, Admin")]
        [HttpPost("restore/{id}")]
        public IActionResult Restore(int id)
        {
            postService.Restore(id);
            return Ok("Post restored");
        }

        [HttpDelete("delete/{id}")]
        public IActionResult Delete(int id)
        {
            var userRole = User.GetUserRole();
            if (userRole == "User")
            {
                var userId = User.GetUserID();
                postService.DeleteAsUser(id, userId);
                return Ok("Post deleted");
            }

            postService.Delete(id);
            return Ok("Post deleted");

        }
    }
}
