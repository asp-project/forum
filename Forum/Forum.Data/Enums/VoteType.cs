﻿namespace Forum.Data.Enums
{
    public enum VoteType
    {
        Downvote = -1,
        None = 0,
        Upvote = 1,
    }
}
