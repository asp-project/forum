﻿using Forum.Data.Contracts;
using System.Linq;

namespace Forum.Repository.Contacts
{
    public interface IRepository<TEntity> where TEntity : class, IEntity
    {
        IQueryable<TEntity> QueryAll();

        IQueryable<TEntity> QueryAllWithDeleted();

        void Add(TEntity entity);

        void Update(TEntity entity);

        void Remove(TEntity entity);

        void Restore(TEntity entity);
    }
}
