﻿using Forum.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Forum.Repository.Extensions
{
    public static class CollectionIncludesExtension
    {
        public static IQueryable<Category> IncludeCategoryBasics(this IQueryable<Category> source)
        {
            if (source == null)
            {
                throw new ArgumentNullException();
            }

            return source
                .Include(c => c.Author)
                .Include(c => c.Posts);
        }

        public static IQueryable<User> IncludeUserBasics(this IQueryable<User> source)
        {
            if (source == null)
            {
                throw new ArgumentNullException();
            }

            return source
                    .Include(user => user.Comments)
                    .Include(user => user.Role)
                    .Include(user => user.Categories)
                    .Include(user => user.Votes)
                    .Include(user => user.Posts);
        }

        public static IQueryable<Post> IncludePostBasics(this IQueryable<Post> source)
        {
            if (source == null)
            {
                throw new ArgumentNullException();
            }

            return source
                    .Include(p => p.Author)
                    .Include(p => p.Category)
                    .Include(p => p.Comments)
                    .Include(p => p.Votes);
        }

        public static IQueryable<Comment> IncludeCommentBasics(this IQueryable<Comment> source)
        {
            if (source == null)
            {
                throw new ArgumentNullException();
            }

            return source
                        .Include(c => c.Author)
                        .Include(c => c.Post)
                        .Include(c => c.Votes);
        }
    }
}
