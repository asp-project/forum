﻿using Forum.Data.Models;
using Forum.Repository.Models;
using Forum.Service.DTOs;
using System.Linq;

namespace Forum.Service.Contracts
{
    public interface ICategoryService : IService<Category>
    {
        void Create(Category category);

        IQueryable<CategoryDTO> GetAll(Pagination pagination, bool includeDeleted = false);

        CategoryDTO GetById(int id, bool includeDeleted = false);
    }
}
