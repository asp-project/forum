﻿using Forum.Data;
using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Extensions;
using Forum.Repository.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Forum.Repository.Types
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ForumDbContext context) : base(context)
        {

        }
        public override IQueryable<Category> QueryAll()
        {
            return base.QueryAll()
                .IncludeCategoryBasics()
                .AsSplitQuery();
        }
        public override IQueryable<Category> QueryAllWithDeleted()
        {
            return base.QueryAllWithDeleted()
                .IncludeCategoryBasics()
                .AsSplitQuery();
        }
        public IQueryable<Category> QueryAll(Pagination pagination)
        {
            return base.QueryAll().AddPagination(pagination)
                .IncludeCategoryBasics()
                .AsSplitQuery();
        }

        public IQueryable<Category> QueryAllWithDeleted(Pagination pagination)
        {
            return base.QueryAll().AddPagination(pagination)
                .IncludeCategoryBasics()
                .AsSplitQuery();
        }
    }
}
