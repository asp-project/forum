﻿using Forum.Data.Models;
using System;

namespace Forum.Service.DTOs
{
    public class UserDTO
    {
        public UserDTO()
        {

        }

        public UserDTO(User user)
        {
            this.Id = user.Id;
            this.Email = user.Email;
            this.RoleName = user.Role.RoleName;
            this.DisplayName = user.DisplayName;
            this.UpdatedOn = user.UpdatedOn;
            this.CreatedOn = user.CreatedOn;
            this.TotalPosts = user.Posts.Count;
            this.TotalComments = user.Comments.Count;
        }
        public int Id { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }
        public string DisplayName { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int TotalPosts { get; set; }
        public int TotalComments { get; set; }
    }
}
