﻿using Forum.Service.Contracts;
using Forum.WebAPI.Extension;
using Forum.WebAPI.Models;
using Forum.WebAPI.Models.ModelMapper;
using Forum.WebAPI.Models.WebModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Forum.WebAPI.Controllers.API
{
    //   [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService commentService;

        public CommentsController(ICommentService commentService)
        {
            this.commentService = commentService;
        }

        // GET: api/comments
        [HttpGet]
        public IActionResult GetAll([FromQuery] bool withDeleted, [FromQuery] PaginationModel paginationModel)
        {
            var pagination = ModelMapper.ToModel(paginationModel);
            var result = commentService.GetAll(pagination, withDeleted);
            return Ok(result);

        }

        [HttpGet("{id}")]
        public IActionResult Get(int id, bool withDeleted)
        {

            var result = commentService.GetById(id, withDeleted);
            return Ok(result);

        }

        // POST: api/comments
        [HttpPost("create")]
        public IActionResult Create([FromBody] CommentWebModel model)
        {
            var comment = ModelMapper.ToModel(model);
            //var userId = User.GetUserID();
            commentService.AddCommentToPost(comment, 1);

            return Created("Posted a comment", model);
        }

        [HttpPost("update/{id}")]
        public IActionResult Update(int id, [FromBody] CommentUpdateModel model)
        {
            var userId = User.GetUserID();

            var comment = ModelMapper.ToModel(model);
            commentService.Update(id, comment, userId);

            return Ok("Comment updated");
        }

        [Authorize(Roles = "Moderator, Admin")]
        [HttpPost("restore/{id}")]
        public IActionResult Restore(int id)
        {
            commentService.Restore(id);
            return Ok("Comment restored.");
        }

        [HttpDelete("delete/{id}")]
        public IActionResult Delete(int id)
        {
            var userRole = User.GetUserRole();
            if (userRole == "User")
            {
                var userId = User.GetUserID();
                commentService.DeleteAsUser(id, userId);
            }
            commentService.Delete(id);
            return Ok("Comment deleted");
        }
    }
}
