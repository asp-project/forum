﻿using Forum.Data.Abstraction;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Data.Models
{
    public class Post : Entity
    {

        [Required]
        [StringLength(Constants.CATEGORY_TITLE_MAX_LEN)]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        public int VoteCount { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? UpdatedOn { get; set; }

        public int AuthorId { get; set; }
        public virtual User Author { get; set; }

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public virtual ICollection<PostVote> Votes { get; set; } = new List<PostVote>();

        public virtual ICollection<Comment> Comments { get; set; } = new List<Comment>();
    }
}
