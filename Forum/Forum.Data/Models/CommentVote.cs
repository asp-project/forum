﻿using Forum.Data.Abstraction;
using Forum.Data.Enums;

namespace Forum.Data.Models
{
    public class CommentVote : Entity
    {
        public VoteType VoteType { get; set; }
        public int CommentId { get; set; }
        public virtual Comment Comment { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
