﻿using Forum.Repository.Models;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace Forum.Repository.Extensions
{
    public static class CollectionExtension
    {
        public static IQueryable<T> AddPagination<T>(this IQueryable<T> source, Pagination pagination)
        {
            if (pagination == null)
            {
                return source;
            }

            return source
                    .Skip(pagination.Offset)
                    .Take(pagination.Limit);
        }

        public static IQueryable<T> AddSort<T>(this IQueryable<T> source, Order order)
        {
            if (source == null || order == null)
            {
                throw new ArgumentNullException();
            }

            source = !order.Desc ? source.OrderBy(order.Field) : source.OrderBy($"{order.Field} Desc");
            return source;
        }
    }
}
