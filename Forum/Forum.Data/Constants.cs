﻿namespace Forum.Data
{
    public static class Constants
    {
        public const int REQ_PASSWORD_LEN = 8;

        public const int CATEGORY_TITLE_MAX_LEN = 200;

        public const int POST_TITLE_MAX_LEN = 200;
        public const int NAME_MIN_LEN = 2;
        public const int NAME_MAX_LEN = 20;
    }
}
