﻿using Forum.Data.Contracts;

namespace Forum.Service.Contracts
{
    public interface IService<TEntity>
        where TEntity : class, IEntity
    {
        void Update(int id, TEntity entity, int userId);

        void Delete(int id);

        void Restore(int id);
    }
}
