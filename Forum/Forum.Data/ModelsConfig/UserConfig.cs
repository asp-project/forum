﻿using Forum.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Forum.Data.ModelsConfig
{
    class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasIndex(user => user.Email)
                .IsUnique();

            builder.HasIndex(user => user.Username)
                .IsUnique();

            builder.HasMany(user => user.Categories)
                .WithOne(category => category.Author)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(user => user.Posts)
                .WithOne(post => post.Author)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(user => user.Votes)
                .WithOne(vote => vote.User)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(user => user.Comments)
                .WithOne(comment => comment.Author)
                .OnDelete(DeleteBehavior.Restrict);

            //builder.HasQueryFilter(x => !x.IsDeleted);
        }
    }
}
