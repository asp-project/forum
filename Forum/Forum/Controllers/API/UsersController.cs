﻿using Forum.Service.Contracts;
using Forum.Service.Helpers;
using Forum.WebAPI.Extension;
using Forum.WebAPI.Models;
using Forum.WebAPI.Models.ModelMapper;
using Forum.WebAPI.Models.WebModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Forum.WebAPI.Controllers.API
{
    // [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = userService.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(response);
        }

        // POST: api/[controller]/register
        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Create([FromBody] UserWebModel model)
        {
            var user = ModelMapper.ToModel(model);

            userService.Register(user);

            return Created("Posted", model);
        }


        // GET: api/[controller]
        [Authorize(Roles = "Moderator, Admin")]
        [HttpGet]
        public IActionResult GetAll(bool withDeleted, [FromQuery] PaginationModel paginationModel, string filter)
        {
            var pagination = ModelMapper.ToModel(paginationModel);
            var result = userService.GetAll(pagination, filter, withDeleted);
            return Ok(result);
        }

        [Authorize(Roles = "Moderator, Admin")]
        [HttpGet("{id}")]
        public IActionResult Get(int id, bool withDeleted)
        {

            var result = userService.GetById(id, withDeleted);
            return Ok(result);

        }

        [Authorize(Roles = "Admin")]
        [HttpPost("permissionlevel/{id}")]
        public IActionResult SetPermissionLevel(int id)
        {
            userService.SetPermissionLevel(id);
            return Ok();
        }

        [Authorize(Roles = "Moderator, Admin")]
        [HttpPost("blockstatus/{id}")]
        public IActionResult SetBlockStatus(int id)
        {
            userService.SetBlockStatus(id);
            return Ok();
        }

        [HttpPost("update")]
        public IActionResult Update([FromBody] UserUpdateModel model)
        {
            var userId = User.GetUserID();
            var user = ModelMapper.ToModel(model);
            userService.Update(userId, user, userId);

            return Ok();
        }

        [Authorize(Roles = "Moderator, Admin")]
        [HttpPost("restore/{id}")]
        public IActionResult Restore(int id)
        {
            userService.Restore(id);
            return Ok();
        }

        [HttpDelete("delete")]
        public IActionResult Delete()
        {
            var userId = User.GetUserID();
            userService.Delete(userId);
            return NoContent();
        }
    }
}
