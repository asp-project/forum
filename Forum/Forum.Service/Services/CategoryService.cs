﻿using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Models;
using Forum.Service.Contracts;
using Forum.Service.DTOs;
using Forum.Service.Exceptions;
using System;
using System.Linq;

namespace Forum.Service.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IPostRepository postsRepository;
        private readonly ICategoryRepository categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository,
            IPostRepository postRepository)
        {
            this.categoryRepository = categoryRepository;
            this.postsRepository = postRepository;
        }

        public IQueryable<CategoryDTO> GetAll(Pagination pagination, bool includeDeleted = false)
        {
            if (!includeDeleted)
            {
                var categories = this.categoryRepository.QueryAll(pagination)
                    .Select(c => new CategoryDTO(c));
                if (!categories.Any())
                {
                    throw new EntityNotFoundException("There are no categories yet.");
                }
                return categories;
            }
            else
            {
                var categories = this.categoryRepository.QueryAllWithDeleted(pagination)
                    .Select(c => new CategoryDTO(c));

                if (!categories.Any())
                {
                    throw new EntityNotFoundException("There are no categories yet.");
                }
                return categories;
            }
        }

        public CategoryDTO GetById(int id, bool includeDeleted = false)
        {
            if (!includeDeleted)
            {
                var category = this.categoryRepository.QueryAll().ToList()
                    .FirstOrDefault(x => x.Id == id);

                if (category == null)
                {
                    throw new EntityNotFoundException("There is no such category.");
                }

                var dto = new CategoryDTO()
                {
                    Id = category.Id,
                    AuthorId = category.AuthorId,
                    AuthorName = category.Author.DisplayName,
                    CreatedOn = category.CreatedOn,
                    Title = category.Title
                };

                return dto;
            }
            else
            {
                var category = this.categoryRepository.QueryAllWithDeleted()
                     .FirstOrDefault(x => x.Id == id);

                if (category == null)
                {
                    throw new EntityNotFoundException("There is no such category.");
                }
                var dto = new CategoryDTO()
                {
                    Id = category.Id,
                    AuthorId = category.AuthorId,
                    AuthorName = category.Author.DisplayName,
                    CreatedOn = category.CreatedOn,
                    Title = category.Title
                };
                return dto;
            }
        }

        public virtual void Delete(int id)
        {
            var entity = this.categoryRepository.QueryAll()
                .Where(x => x.Id == id)
                .FirstOrDefault();

            if (entity == null)
            {
                throw new EntityNotFoundException("There is no such category.");
            }

            foreach (var post in entity.Posts)
            {
                this.postsRepository.Remove(post);
            }

            //this.postRepository is called
            this.categoryRepository.Remove(entity);
        }

        public void Restore(int id)
        {
            var entity = this.categoryRepository.QueryAll()
                .FirstOrDefault(x => x.Id == id);

            if (entity == null)
            {
                throw new EntityNotFoundException("There is no such category.");
            }

            this.categoryRepository.Restore(entity);
        }

        public void Create(Category category)
        {
            //Assert.Should be called / times 
            this.categoryRepository.Add(category);
        }

        void IService<Category>.Update(int id, Category entity, int userId)
        {
            //
            var categoryToUpdate = this.categoryRepository.QueryAll().FirstOrDefault(c => c.Id == id);

            if (categoryToUpdate == null)
            {
                throw new EntityNotFoundException("There is no such category.");
            }
            if (categoryToUpdate.AuthorId != userId)
            {
                throw new Exception("You can only edit your categories");
            }

            categoryToUpdate.Title = entity.Title;
            this.categoryRepository.Update(categoryToUpdate);
        }
    }
}
