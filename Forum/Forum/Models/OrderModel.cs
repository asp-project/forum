﻿using Microsoft.AspNetCore.Mvc;

namespace Forum.WebAPI.Models
{
    public class OrderModel
    {
        [FromQuery(Name = "descending")]
        public bool Desc { get; set; }
        [FromQuery(Name = "field")]
        public string Field { get; set; }
    }
}
