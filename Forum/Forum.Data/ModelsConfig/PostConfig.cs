﻿using Forum.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Forum.Data.ModelsConfig
{
    class PostConfig : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.HasOne(post => post.Author)
                .WithMany(author => author.Posts)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(post => post.Category)
                   .WithMany(c => c.Posts)
                   .HasForeignKey(c => c.CategoryId)
                   .OnDelete(DeleteBehavior.Restrict);

            //builder.HasQueryFilter(x => !x.IsDeleted);
        }
    }
}
