﻿using Forum.Data.Models;
using Forum.Repository.Models;
using System.Linq;

namespace Forum.Repository.Contacts
{
    public interface ICommentRepository : IRepository<Comment>
    {
        public IQueryable<Comment> QueryAll(Pagination pagination = default);

        public IQueryable<Comment> QueryAllWithDeleted(Pagination pagination = default);
    }
}
