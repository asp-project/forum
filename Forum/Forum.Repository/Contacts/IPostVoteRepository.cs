﻿using Forum.Data.Models;
namespace Forum.Repository.Contacts
{
    public interface IPostVoteRepository : IRepository<PostVote>
    {
    }
}
