﻿namespace Forum.Repository.Models
{
    public class Pagination
    {
        public Pagination(int offset = 0, int limit = 10)
        {
            Offset = offset < 0 ? 0 : offset;
            Limit = limit < 1 || limit > 10 ? 10 : limit;
        }
        public int Offset { get; set; }
        public int Limit { get; set; }
    }
}
