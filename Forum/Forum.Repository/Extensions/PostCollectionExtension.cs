﻿using Forum.Data.Models;
using Forum.Repository.Models;
using System.Linq;

namespace Forum.Repository.Extensions
{
    public static class PostCollectionExtension
    {
        public static IQueryable<Post> FilterPost(this IQueryable<Post> source, PostFilter filter)
        {
            if (filter == null)
            {
                return source;
            }
            else
            {
                return source
                    .FilterByName(filter.AuthorName)
                    .FilterByTitle(filter.Title)
                    .FilterByContent(filter.Content);
            }
        }

        private static IQueryable<Post> FilterByName(this IQueryable<Post> source, string name)
        {
            if (string.IsNullOrEmpty(name)) return source;
            return source.Where(post => post.Author.DisplayName.Contains(name));
        }
        private static IQueryable<Post> FilterByTitle(this IQueryable<Post> source, string title)
        {
            if (string.IsNullOrEmpty(title)) return source;
            return source.Where(post => post.Title.Contains(title));
        }
        private static IQueryable<Post> FilterByContent(this IQueryable<Post> source, string content)
        {
            if (string.IsNullOrEmpty(content)) return source;
            return source.Where(post => post.Content.Contains(content));
        }
    }
}
