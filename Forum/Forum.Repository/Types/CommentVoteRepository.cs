﻿using Forum.Data;
using Forum.Data.Models;
using Forum.Repository.Contacts;

namespace Forum.Repository.Types
{
    public class CommentVoteRepository : Repository<CommentVote>, ICommentVoteRepository
    {
        public CommentVoteRepository(ForumDbContext context) : base(context)
        {

        }
    }
}
