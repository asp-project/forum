﻿using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Models;
using Forum.Service.Exceptions;
using Forum.Service.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Tests.ServiceTests
{
    [TestClass]
    public class CommentServiceTest
    {
        private readonly CommentService sut;
        private List<Comment> testList;

        private Pagination pagination = new Pagination(0, int.MaxValue);

        private readonly Mock<ICommentRepository> commentRepository = new Mock<ICommentRepository>();
        private readonly Mock<IPostRepository> postRepository = new Mock<IPostRepository>();
        private readonly Mock<IUserRepository> userRepository = new Mock<IUserRepository>();

        public CommentServiceTest()
        {
            sut = new CommentService(this.commentRepository.Object, this.postRepository.Object, this.userRepository.Object);
            testList = new List<Comment>()
            {
                new Comment
                {
                    Id = 2,
                    AuthorId = 1,
                    Author = new User()
                    {
                        Username = "Alexandro"
                    },
                    CreatedOn = DateTime.Now,
                    Content = "Test Content",
                    Post = new Post()
                    {
                        Id = 3,
                    },
                    PostId = 3
                },
                new Comment
                {
                    Id = 4,
                    AuthorId = 1,
                    Author = new User()
                    {
                        Username = "DelPiero"
                    },
                    CreatedOn = DateTime.Now,
                    Content = "Test Content",
                    Post = new Post()
                    {
                        Id = 5,
                    },
                    PostId = 5
                }

            };

        }

        [TestMethod]
        public void CommentsShould_ReturnAllComments_WithoutDeleted()
        {
            //Arrange           
            var includeDeleted = false;
            commentRepository.Setup(x => x.QueryAll(this.pagination)).Returns(testList.AsQueryable());

            //Act
            var result = sut.GetAll(this.pagination, includeDeleted);

            //Assert
            Assert.AreEqual(testList.Count, result.ToList().Count());

        }


        [TestMethod]
        public void CommentsShould_ThrowExceptionWhen_CommentsNotAdded()
        {
            //Arrange
            testList = new List<Comment>();
            commentRepository.Setup(x => x.QueryAll(this.pagination)).Returns(testList.AsQueryable());

            //Act & Assert
            var e = Assert.ThrowsException<EntityNotFoundException>(() => sut.GetAll(this.pagination));
            Assert.AreSame(e.Message, "There is no such comment.");

        }
        [TestMethod]
        public void CommentsShould_ReturnAllComments_IncludeDeleted()
        {
            //Arrange
            var includeDeleted = true;
            commentRepository.Setup(x => x.QueryAllWithDeleted(this.pagination)).Returns(testList.AsQueryable());

            //Act
            var result = sut.GetAll(this.pagination, includeDeleted);

            //Assert
            Assert.AreEqual(testList.Count, result.ToList().Count());

        }

        [TestMethod]
        public void CommentsShould_ThrowExceptionWhen_CommentsNotAdded_IncludeDeleted()
        {
            //Arrange
            var includeDeleted = true;
            testList = new List<Comment>();
            commentRepository.Setup(x => x.QueryAllWithDeleted(this.pagination)).Returns(testList.AsQueryable());

            //Act & Assert
            var e = Assert.ThrowsException<EntityNotFoundException>(() => sut.GetAll(this.pagination, includeDeleted));
            Assert.AreSame(e.Message, "There is no such comment.");

        }
    }
}
