﻿using System.ComponentModel.DataAnnotations;

namespace Forum.WebAPI.Models.WebModels
{
    public class CommentUpdateModel
    {
        [Required]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Content { get; set; }
    }
}
