﻿using Forum.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Forum.Tests
{
    public abstract class BaseTest
    {
        [TestCleanup]
        public void Cleanup()
        {
            var options = Utilities.GetOptions(nameof(TestContext.TestName));
            var context = new ForumDbContext(options);
            context.Database.EnsureDeleted();
        }
    }
}
