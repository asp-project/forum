﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Service.Helpers
{
    public class AuthenticateRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
