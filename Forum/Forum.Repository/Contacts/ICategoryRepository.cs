﻿using Forum.Data.Models;
using Forum.Repository.Models;
using System.Linq;

namespace Forum.Repository.Contacts
{
    public interface ICategoryRepository : IRepository<Category>
    {
        public IQueryable<Category> QueryAll(Pagination pagination = default);

        public IQueryable<Category> QueryAllWithDeleted(Pagination pagination = default);
    }
}
