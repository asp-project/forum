﻿

using Forum.Data.Models;

namespace Forum.Service.Helpers
{
    public class AuthenticateResponse
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }


        public AuthenticateResponse(User user, string token)
        {
            Id = user.Id;
            DisplayName = user.DisplayName;
            Email = user.Email;
            Role = user.Role.RoleName;
            Username = user.Username;
            Token = token;
        }
    }
}
