﻿using Forum.Data.Models;
using System;

namespace Forum.Service.DTOs
{
    public class CategoryDTO
    {
        public CategoryDTO()
        {

        }
        public CategoryDTO(Category category)
        {
            Id = category.Id;
            Title = category.Title;
            AuthorId = category.AuthorId;
            AuthorName = category.Author.Username;
            CreatedOn = category.CreatedOn;
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public int AuthorId { get; set; }

        public string AuthorName { get; set; }

        public DateTime? CreatedOn { get; set; }
    }
}
