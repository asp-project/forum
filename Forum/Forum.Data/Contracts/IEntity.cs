﻿using System;

namespace Forum.Data.Contracts
{
    public interface IEntity
    {
        int Id { get; set; }
        DateTime? CreatedOn { get; set; }

        DateTime? DeletedOn { get; set; }

        bool IsDeleted { get; set; }
    }
}
