﻿using Forum.MVC.Attributes;
using Forum.MVC.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Forum.MVC.Controllers
{

    public class CategoriesController : Controller
    {
        private readonly ICategoryRepository categoryRepository;
        private readonly IPostRepository postRepository;
        private readonly IPostVoteRepository voteRepository;
        private readonly ICommentRepository commentRepository;
        public CategoriesController(ICategoryRepository categoryRepository,
                                   IPostRepository postRepository,
                                   IPostVoteRepository voteRepository,
                                   ICommentRepository commentRepository)

        {
            this.categoryRepository = categoryRepository;
            this.postRepository = postRepository;
            this.voteRepository = voteRepository;
            this.commentRepository = commentRepository;
        }

        public IActionResult Index([FromQuery] string category, int page)
        {
            if (this.HttpContext.Session.GetString("Token") == null)
            {
                return this.PartialView("Error", "Psst.. Login first ;)");
            }

            var forum = ForumModel(category, page);
            return View(forum);

        }

        [MyAuthorization]
        public IActionResult Comment([FromQuery] string category)
        {
            var forum = ForumModel(category);
            return View(forum);
        }

        [MyAuthorization]
        public IActionResult Post([FromQuery] string category, [FromQuery] int id)
        {
            var forum = ForumModel(category);
            forum.PostId = this.postRepository.QueryAll().Where(p => p.Id == id).FirstOrDefault().Id;
            this.HttpContext.Session.SetInt32("PostId", (int)forum.PostId);
            return View(forum);
        }

        public IActionResult Error(string message)
        {
            return PartialView(message);
        }

        public ForumModel ForumModel([FromQuery] string category)
        {
            var order = new Order(true, "CreatedOn");
            var pagination = new Pagination(0, 3);
            var filter = new PostFilter();
            var categories = this.categoryRepository.QueryAll();
            var posts = this.postRepository.QueryAll()
                                        .Where(x => x.Category.Title == $"{category}")
                                        .Include(x => x.Comments)
                                        .AsSplitQuery();

            var votes = this.voteRepository.QueryAll();

            var comments = this.commentRepository.QueryAll();


            var forum = new ForumModel()
            {
                Comments = comments,
                Votes = votes,
                CategoryName = category,
                Categories = categories,
                Posts = posts
            };

            return forum;
        }

        public ForumModel ForumModel([FromQuery] string category, int page)
        {
            int offset = 0;
            if (page != 1)
            {
                offset = 5 * (page - 1);
            }
            int limit = 5;
            var order = new Order(true, "CreatedOn");
            var pagination = new Pagination(offset, limit);
            var filter = new PostFilter();
            var categories = this.categoryRepository.QueryAll();
            int pages = this.postRepository.QueryAll().Where(x => x.Category.Title == $"{category}").Count();
            if (pages % 5 == 0)
            {
                pages /= 5;
            }
            else
            {
                pages = (pages / 5) + 1;
            }
            var posts = this.postRepository.QueryAllWithOptions(order, pagination, filter)
                                      .Where(x => x.Category.Title == $"{category}")
                                      .Include(x => x.Comments)
                                      .AsSplitQuery()
                                      .Include(x => x.Votes)
                                      .AsSplitQuery();

            var comments = this.commentRepository.QueryAll();

            var votes = this.voteRepository.QueryAll();

            var forum = new ForumModel()
            {
                Votes = votes,
                Comments = comments,
                Pages = pages,
                CategoryName = category,
                Categories = categories,
                Posts = posts,
                PageNumber = page


            };

            return forum;
        }

    }

}
