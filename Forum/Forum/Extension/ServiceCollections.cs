﻿using Forum.Repository.Contacts;
using Forum.Repository.Types;
using Forum.Service.Contracts;
using Forum.Service.Services;
using Microsoft.Extensions.DependencyInjection;


namespace Forum.WebAPI.Extension

{
    public static class ServiceCollections
    {
        public static void ForumBase(this IServiceCollection services)
        {
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            services.AddScoped<IPostVoteRepository, PostVoteRepository>();
            services.AddScoped<ICommentVoteRepository, CommentVoteRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IVoteService, VoteService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPostService, PostService>();
        }
    }
}
