﻿using Forum.Data.Contracts;
using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.Data.Abstraction
{
    public abstract class Entity : IEntity
    {
        [Key]
        public int Id { get; set; }
        public bool IsDeleted { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? DeletedOn { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? CreatedOn { get; set; }
    }
}
