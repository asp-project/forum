﻿using Forum.Data.Models;
using System.Linq;

namespace Forum.Repository.Extensions
{
    public static class UserCollectionExtension
    {
        public static IQueryable<User> FilterUser(this IQueryable<User> source, string filter)
        {
            if (filter == null)
            {
                return source;
            }
            else
            {
                return source.Where(user => user.Username.Contains(filter)
                || user.DisplayName.Contains(filter)
                || user.Email.Contains(filter));
            }
        }
    }
}
