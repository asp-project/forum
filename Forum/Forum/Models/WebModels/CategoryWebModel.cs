﻿using Forum.Data;
using System.ComponentModel.DataAnnotations;

namespace Forum.WebAPI.Models.WebModels
{
    public class CategoryWebModel
    {
        [Required]
        [StringLength(Constants.CATEGORY_TITLE_MAX_LEN, MinimumLength = 4, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string Title { get; set; }
    }
}
