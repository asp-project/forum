﻿using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Models;
using Forum.Service.Contracts;
using Forum.Service.DTOs;
using Forum.Service.Exceptions;
using Forum.Service.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Forum.Service.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly AppSettings appSettings;

        public UserService(IUserRepository userRepository,
            IOptions<AppSettings> appSettings)
        {
            this.userRepository = userRepository;
            this.appSettings = appSettings.Value;
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = this.userRepository.QueryAll()
                        .SingleOrDefault(x => x.Username == model.Username && x.Password == model.Password);

            // return null if user not found
            if (user == null) throw new EntityNotFoundException("Wrong username/password. Try again.");

            // authentication successful so generate jwt token
            var token = GenerateJwtToken(user);

            return new AuthenticateResponse(user, token);
        }


        public AuthenticateResponse Register(User user)
        {
            this.userRepository.Add(user);

            var token = GenerateJwtToken(user);

            return new AuthenticateResponse(user, token);
        }

        public IQueryable<UserDTO> GetAll(Pagination pagination, string filter, bool includeDeleted = false)
        {
            if (!includeDeleted)
            {
                var users = this.userRepository.QueryAll(pagination, filter)
                    .Select(user => new UserDTO(user));

                if (!users.Any())
                {
                    throw new EntityNotFoundException("There are no users!");
                }
                return users;
            }
            else
            {
                var users = this.userRepository.QueryAllWithDeleted(pagination, filter)
                    .Select(user => new UserDTO(user));

                if (!users.Any())
                {
                    throw new EntityNotFoundException("There are no users!");
                }
                return users;
            }
        }

        public IQueryable<UserDTO> GetById(int id, bool includeDeleted = false)
        {
            if (!includeDeleted)
            {
                var user = this.userRepository.QueryAll()
                    .Where(x => x.Id == id)
                    .Select(user => new UserDTO(user));

                if (!user.Any())
                {
                    throw new EntityNotFoundException("There is no such user!");
                }
                return user;
            }
            else
            {
                var user = this.userRepository.QueryAllWithDeleted()
                    .Where(x => x.Id == id)
                    .Select(user => new UserDTO(user));

                if (!user.Any())
                {
                    throw new EntityNotFoundException("There is no such user!");
                }
                return user;
            }
        }

        public void Delete(int id)
        {
            var entity = this.userRepository.QueryAll()
                        .Where(x => x.Id == id)
                        .FirstOrDefault();

            if (entity == null)
            {
                throw new EntityNotFoundException("There is no such user.");
            }

            this.userRepository.Remove(entity);
        }

        public void Restore(int id)
        {
            var entity = this.userRepository.QueryAll()
                            .FirstOrDefault(x => x.Id == id);

            if (entity == null)
            {
                throw new EntityNotFoundException("There is no such user.");
            }

            this.userRepository.Restore(entity);
        }

        //TODO generate new token when changing any credentials
        public void Update(int id, User entity, int userId)
        {
            var userToUpdate = this.userRepository.QueryAll().FirstOrDefault(user => user.Id == id);

            if (userToUpdate == null)
            {
                throw new EntityNotFoundException("There is no such user.");
            }

            userToUpdate.DisplayName = entity.DisplayName;
            userToUpdate.Password = entity.Password;
            userToUpdate.Email = entity.Email;
            //userToUpdate.ProfileUrl = entity.ProfileUrl;
            this.userRepository.Update(userToUpdate);
        }

        public void SetPermissionLevel(int id)
        {
            var user = this.userRepository.QueryAll()
                             .FirstOrDefault(user => user.Id == id);
            if (user == null)
            {
                throw new EntityNotFoundException("There is no such user.");
            }

            if (user.RoleId == 1) user.RoleId = 2;
            else if (user.RoleId == 2) user.RoleId = 1;

            this.userRepository.Update(user);
        }

        public void SetBlockStatus(int id)
        {
            var user = this.userRepository.QueryAll()
                        .FirstOrDefault(user => user.Id == id);
            if (user == null)
            {
                throw new EntityNotFoundException("There is no such user");
            }

            if (user.IsBlocked == true) user.IsBlocked = false;
            else user.IsBlocked = true;

            this.userRepository.Update(user);
        }

        private string GenerateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim("id", user.Id.ToString()),
                    new Claim("role", user.Role.RoleName),
                    new Claim("username", user.Username),
                    new Claim("email", user.Email),
                    new Claim("displayname", user.DisplayName)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
