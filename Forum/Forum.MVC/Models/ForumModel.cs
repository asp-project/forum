﻿using Forum.Data.Models;
using Forum.WebAPI.Models.WebModels;
using System.Collections.Generic;

namespace Forum.MVC.Models
{
    public class ForumModel
    {
#pragma warning disable CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.

        public int? UserId { get; set; }
        public int? Pages { get; set; }

        public bool? CommentVote { get; set; }
        public int? PageNumber { get; set; }
        public string? CategoryName { get; set; }

        public int? CategoryId { get; set; }

        public int? PostsCount { get; set; }

        public int? PostId { get; set; }

        public IEnumerable<Category>? Categories { get; set; }

        public IEnumerable<Comment>? Comments { get; set; }

        public IEnumerable<Post>? Posts { get; set; }

        public IEnumerable<User>? Users { get; set; }

        public IEnumerable<PostVote>? Votes { get; set; }

        public PostWebModel? PostWebModel { get; set; }

        public CommentWebModel? CommentWebModel { get; set; }

        public Post? PostPageModel { get; set; }


#pragma warning restore CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.



    }
}
