﻿using Forum.Data;
using Forum.Data.Contracts;
using Forum.Repository.Contacts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Forum.Repository
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        private readonly DbSet<TEntity> entities;
        private readonly ForumDbContext context;
        public Repository(ForumDbContext context)
        {
            this.context = context;
            this.entities = context.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> QueryAll()
        {
            return this.entities
                .Where(e => !e.IsDeleted)
                .OrderByDescending(e => e.CreatedOn);
        }

        public virtual IQueryable<TEntity> QueryAllWithDeleted()
        {
            return this.entities
                .OrderByDescending(e => e.CreatedOn);
        }

        public void Add(TEntity entity)
        {
            this.entities.Add(entity);
            this.context.SaveChanges();
        }
        public void Update(TEntity entity)
        {
            this.context.SaveChanges();
        }

        public void Remove(TEntity entity)
        {
            entity.IsDeleted = true;
            entity.DeletedOn = DateTime.Now;
            this.context.SaveChanges();
        }

        public void Restore(TEntity entity)
        {
            entity.IsDeleted = false;
            entity.DeletedOn = null;
            this.context.SaveChanges();
        }
    }
}
