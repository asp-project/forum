﻿using Forum.Data;
using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Extensions;
using Forum.Repository.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Forum.Repository.Types
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ForumDbContext context) : base(context)
        {
        }

        public override IQueryable<User> QueryAll()
        {
            return base.QueryAll()
                .IncludeUserBasics()
                .AsSplitQuery();
        }
        public override IQueryable<User> QueryAllWithDeleted()
        {
            return base.QueryAllWithDeleted()
                .IncludeUserBasics()
                .AsSplitQuery();
        }
        public IQueryable<User> QueryAll(Pagination pagination, string filter)
        {
            return base.QueryAll()
                    .FilterUser(filter)
                    .AddPagination(pagination)
                    .IncludeUserBasics()
                .AsSplitQuery();
        }

        public IQueryable<User> QueryAllWithDeleted(Pagination pagination, string filter)
        {
            return base.QueryAllWithDeleted()
                    .FilterUser(filter)
                    .AddPagination(pagination)
                    .IncludeUserBasics()
                .AsSplitQuery();
        }
    }
}
