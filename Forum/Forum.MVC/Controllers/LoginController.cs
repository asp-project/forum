﻿using Forum.Data.Models;
using Forum.MVC.Models;
using Forum.Repository.Contacts;
using Forum.Service.Contracts;
using Forum.Service.Exceptions;
using Forum.Service.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Forum.MVC.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUserRepository userRepository;
        private readonly AppSettings appSettings;
        private readonly IUserService userService;

        public LoginController(IUserRepository userRepository,
            IUserService userService,
            IOptions<AppSettings> appSettings)
        {
            this.userRepository = userRepository;
            this.userService = userService;
            this.appSettings = appSettings.Value;
        }
        public IActionResult Credentials()
        {
            return PartialView();
        }

        public IActionResult Register()
        {
            try
            {
                var user = new User();
                return PartialView(user);
            }
            catch (Exception ex)
            {
                return this.Error(ex.Message);
            }
        }
        public IActionResult RegisterUser(User user)
        {
            var exception = this.Message(user);
            if (exception != null)
            {
                var url = $"https://localhost:5001/Login/Error?message={exception}";
                return this.Redirect(url);
            }

            else
            {
                this.userRepository.Add(user);
                var userCreated = this.userRepository.QueryAll().Where(x => x.Email == user.Email).FirstOrDefault();
                var token = GenerateJwtToken(user);
                var authenticateResponse = new AuthenticateResponse(userCreated, token);
                this.HttpContext.Session.SetString("Token", $"Bearer {authenticateResponse.Token}");
                this.HttpContext.Session.SetString("DisplayName", $"{authenticateResponse.DisplayName}");
                this.HttpContext.Session.SetString("Email", $"{authenticateResponse.Email}");
                this.HttpContext.Session.SetString("Username", $"{authenticateResponse.Username}");
                this.HttpContext.Session.SetString("Role", $"{authenticateResponse.Role}");
                this.HttpContext.Session.SetInt32("Id", authenticateResponse.Id);

                return PartialView(userCreated);
            }

        }

        public string Message(User user)
        {
            if (user.Email != null && user.DisplayName != null && user.Username != null && user.Password != null)
            {
                return null;
            }
            string message = "Please provide ";
            var list = new List<string>();
            if (user.Email == null)
            {
                list.Add("email");
            }
            if (user.DisplayName == null)
            {
                list.Add("nickname");
            }
            if (user.Username == null)
            {
                list.Add("username");
            }
            if (user.Password == null)
            {
                list.Add("password");
            }
            message += string.Join(',', list);
            return message;
        }
        public IActionResult Logout()
        {
            this.HttpContext.Session.Clear();
            return this.Redirect("https://localhost:5001/home");
        }

        public IActionResult WrongUsername()
        {
            return PartialView();
        }
        public IActionResult SendCredentials(AuthenticateRequest model)
        {

            try
            {
                var user = this.userRepository.QueryAll()
                                    .SingleOrDefault(x => x.Username == model.Username && x.Password == model.Password);

                // return null if user not found
                if (user == null) throw new EntityNotFoundException();

                // authentication successful so generate jwt token
                var token = GenerateJwtToken(user);

                var authenticateResponse = new AuthenticateResponse(user, token);
                this.HttpContext.Session.SetString("Token", $"Bearer {authenticateResponse.Token}");
                this.HttpContext.Session.SetString("DisplayName", $"{authenticateResponse.DisplayName}");
                this.HttpContext.Session.SetString("Email", $"{authenticateResponse.Email}");
                this.HttpContext.Session.SetString("Username", $"{authenticateResponse.Username}");
                this.HttpContext.Session.SetString("Role", $"{authenticateResponse.Role}");
                this.HttpContext.Session.SetInt32("Id", authenticateResponse.Id);

                return this.PartialView(user);
            }
            catch
            {
                return this.Redirect(nameof(WrongUsername));
            }

        }

        public IActionResult Error([FromQuery] string message)
        {
            ErrorViewModel error = new ErrorViewModel();
            error.Message = message;
            return PartialView(error);
        }

        private string GenerateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim("id", user.Id.ToString()),
                    new Claim("role", user.Role.RoleName),
                    new Claim("username", user.Username),
                    new Claim("email", user.Email),
                    new Claim("displayname", user.DisplayName)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
