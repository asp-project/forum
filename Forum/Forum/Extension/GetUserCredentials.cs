﻿using System.Security.Claims;

namespace Forum.WebAPI.Extension
{
    public static class GetUserCredentials
    {
        public static int GetUserID(this ClaimsPrincipal User)
        {
            return int.Parse(User.FindFirstValue("id"));
        }

        public static string GetUserRole(this ClaimsPrincipal User)
        {
            return User.FindFirstValue("role");
        }
    }
}
