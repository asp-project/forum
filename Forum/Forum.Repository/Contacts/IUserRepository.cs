﻿using Forum.Data.Models;
using Forum.Repository.Models;
using System.Linq;

namespace Forum.Repository.Contacts
{
    public interface IUserRepository : IRepository<User>
    {
        public IQueryable<User> QueryAll(Pagination pagination = default, string filter = null);

        public IQueryable<User> QueryAllWithDeleted(Pagination pagination = default, string filter = null);
    }

}
