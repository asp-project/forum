﻿using Forum.MVC.Attributes;
using Forum.MVC.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace Forum.MVC.Controllers
{

    public class HomeController : Controller
    {
        private readonly ICategoryRepository categoryRepository;
        private readonly ICommentRepository commentRepository;
        private readonly IPostRepository postRepository;
        private readonly IUserRepository userRepository;

        public HomeController(ICategoryRepository categoryRepository,
                              ICommentRepository commentRepository,
                              IPostRepository postRepository,
                              IUserRepository userRepository)

        {
            this.categoryRepository = categoryRepository;
            this.commentRepository = commentRepository;
            this.postRepository = postRepository;
            this.userRepository = userRepository;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            var order = new Order();
            var pagination = new Pagination(0, int.MaxValue);
            var forum = new ForumModel()
            {
                CategoryName = "Index",
                Categories = this.categoryRepository.QueryAll().Include(x => x.Posts),
                Comments = this.commentRepository.QueryAll(pagination),
                Posts = this.postRepository.QueryAll(),
                Users = this.userRepository.QueryAll(pagination),
                UserId = this.HttpContext.Session.GetInt32("Id")


            };

            return this.View(forum);
        }
        [MyAuthorization]
        public IActionResult Test()
        {
            return PartialView();
        }

        public IActionResult Error(ErrorViewModel ex)
        {
            return PartialView(ex);
        }

        public IActionResult ErrorMessage([FromQuery] string content)
        {
            ErrorViewModel error = new ErrorViewModel();
            error.Message = content;
            return PartialView(error);
        }
    }
}
