﻿namespace Forum.Repository.Models
{
    public class Order
    {
        public Order(bool order = false, string field = "Id")
        {
            Desc = order;
            Field = field;
        }
        public bool Desc { get; set; }
        public string Field { get; set; }
    }
}
