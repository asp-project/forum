﻿using Forum.Data.Models;
using Forum.Repository.Models;
using Forum.Service.DTOs;
using Forum.Service.Helpers;
using System.Linq;

namespace Forum.Service.Contracts
{
    public interface IUserService : IService<User>
    {
        IQueryable<UserDTO> GetAll(Pagination pagination, string filter, bool includeDeleted = false);

        IQueryable<UserDTO> GetById(int id, bool includeDeleted = false);

        AuthenticateResponse Register(User user);

        void SetPermissionLevel(int id);

        void SetBlockStatus(int id);
        AuthenticateResponse Authenticate(AuthenticateRequest model);
    }
}
