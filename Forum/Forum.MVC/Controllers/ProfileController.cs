﻿using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Service.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Forum.MVC.Controllers
{
    public class ProfileController : Controller
    {
        private readonly IUserRepository userRepository;
        private readonly IUserService userService;

        public ProfileController(IUserRepository userRepository, IUserService userService)

        {
            this.userRepository = userRepository;
            this.userService = userService;
        }

        public IActionResult Index([FromQuery] int id)
        {
            var user = this.userRepository.QueryAll().Where(x => x.Id == id).FirstOrDefault();
            return this.PartialView(user);
        }

        public IActionResult Edit(User user)
        {
            try
            {

                this.userService.Update(user.Id, user, user.Id);
                return this.PartialView();
            }
            catch
            {
                var message = "You missed something?";
                return this.PartialView("Error", message);
            }
        }
    }
}
