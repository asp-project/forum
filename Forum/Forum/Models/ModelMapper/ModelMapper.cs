﻿using Forum.Data.Models;
using Forum.Repository.Models;
using Forum.WebAPI.Models.WebModels;

namespace Forum.WebAPI.Models.ModelMapper
{
    public static class ModelMapper
    {
        public static Category ToModel(CategoryWebModel WebModel)
        {
            var entity = new Category
            {
                Title = WebModel.Title
            };
            return entity;
        }

        public static Post ToModel(PostWebModel WebModel)
        {
            var entity = new Post
            {
                Title = WebModel.Title,
                Content = WebModel.Content,
                CategoryId = WebModel.CategoryId
            };
            return entity;
        }

        public static Post ToModel(PostUpdateModel WebModel)
        {
            var entity = new Post
            {
                Title = WebModel.Title,
                Content = WebModel.Content
            };
            return entity;
        }

        public static Comment ToModel(CommentWebModel WebModel)
        {
            var entity = new Comment
            {
                PostId = WebModel.PostId,
                Content = WebModel.Content
            };
            return entity;
        }

        public static Comment ToModel(CommentUpdateModel WebModel)
        {
            var entity = new Comment
            {
                Content = WebModel.Content
            };
            return entity;
        }
        public static Pagination ToModel(PaginationModel WebModel)
        {
            var entity = new Pagination
            {
                Offset = WebModel.Offset < 0 ? 0 : WebModel.Offset,
                Limit = WebModel.Limit < 1 || WebModel.Limit > 10 ? 10 : WebModel.Limit
            };
            return entity;
        }

        public static Order ToModel(OrderModel model)
        {
            var entity = new Order
            {
                Desc = model.Desc,
                Field = string.IsNullOrEmpty(model.Field) & model.Field != "Id" & model.Field != "CommentCount"
                & model.Field != "VoteCount" ? "Id" : model.Field
            };
            return entity;
        }

        public static PostFilter ToModel(PostFilterQuery query)
        {
            var entity = new PostFilter
            {
                AuthorName = query.AuthorName,
                Title = query.Title,
                Content = query.Content
            };
            return entity;
        }

        public static User ToModel(UserWebModel WebModel)
        {
            var entity = new User
            {
                Username = WebModel.Username,
                Email = WebModel.Email,
                Password = WebModel.Password,
                DisplayName = WebModel.DisplayName
            };
            return entity;
        }

        public static User ToModel(UserUpdateModel WebModel)
        {
            var entity = new User
            {
                Email = WebModel.Email,
                Password = WebModel.Password,
                DisplayName = WebModel.DisplayName
            };
            return entity;
        }
    }
}
