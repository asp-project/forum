﻿using Forum.Data;
using Forum.Data.Models;
using Forum.Repository.Contacts;

namespace Forum.Repository.Types
{
    public class PostVoteRepository : Repository<PostVote>, IPostVoteRepository
    {
        public PostVoteRepository(ForumDbContext context) : base(context)
        {
        }
    }
}
