﻿using Forum.Data.Enums;
using Forum.Service.Contracts;
using Forum.WebAPI.Extension;
using Microsoft.AspNetCore.Mvc;

namespace Forum.WebAPI.Controllers.API
{
    //  [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VotesController : ControllerBase
    {
        private readonly IVoteService voteService;
        public VotesController(IVoteService voteService)
        {
            this.voteService = voteService;
        }

        [HttpGet("commentVote/{commentId}")]
        public IActionResult GetCommentVoteType(int commentId)
        {
            var userId = User.GetUserID();
            var result = voteService.GetUserVoteTypeForCommnet(commentId, userId);
            return Ok($"Your vote for this comment is {result}");
        }

        [HttpGet("postVote/{postId}")]
        public IActionResult GetPostVoteType(int postId)
        {
            var userId = User.GetUserID();
            var result = voteService.GetUserVoteTypeForPost(postId, userId);
            return Ok($"Your vote for this post is {result}");
        }

        [HttpPost("togglePostVote/{postId}")]
        public IActionResult TogglePostVote(int postId, VoteType voteType)
        {
            var userId = User.GetUserID();

            voteService.TogglePostVote(postId, userId, voteType);

            return Ok($"Successfully voted with {voteType}");
        }

        [HttpPost("toggleCommnetVote/{commentId}")]
        public IActionResult ToggleCommentVote(int commentId, VoteType voteType)
        {
            var userId = User.GetUserID();

            voteService.ToggleCommentVote(commentId, userId, voteType);

            return Ok($"Successfully voted with {voteType}");
        }
    }
}
