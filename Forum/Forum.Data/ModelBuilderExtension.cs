﻿using Forum.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Forum.Data
{
    public static class ModelBuilderExtension
    {
        public static IEnumerable<User> Users { get; }

        public static IEnumerable<Role> Roles { get; }

        public static IEnumerable<PostVote> Votes { get; }

        public static IEnumerable<Category> Categories { get; }

        public static IEnumerable<Post> Posts { get; }

        public static IEnumerable<Comment> Comments { get; }

        static ModelBuilderExtension()
        {
            Categories = new HashSet<Category>
            {
                new Category { Id = 1, AuthorId = 127, CreatedOn = DateTime.Now, Title = "Football" },
                new Category { Id = 2, AuthorId = 127, CreatedOn = DateTime.Now, Title = "Basketball" },
                new Category { Id = 3, AuthorId = 127, CreatedOn = DateTime.Now, Title = "Volleyball" },
                new Category { Id = 4, AuthorId = 127, CreatedOn = DateTime.Now, Title = "Tennis" },
                new Category { Id = 5, AuthorId = 127, CreatedOn = DateTime.Now, Title = "Rugby" },
                new Category { Id = 6, AuthorId = 127, CreatedOn = DateTime.Now, Title = "Motocross" },
                new Category { Id = 7, AuthorId = 127, CreatedOn = DateTime.Now, Title = "Formula 1" },
                new Category { Id = 8, AuthorId = 127, CreatedOn = DateTime.Now, Title = "Golf" },
                new Category { Id = 9, AuthorId = 127, CreatedOn = DateTime.Now, Title = "Alpha 31" }

            };

            Roles = new HashSet<Role>
            {
                new Role {Id = 1, RoleName = "Admin"},
                new Role {Id = 2, RoleName = "Moderator"},
                new Role {Id = 3, RoleName = "User"}
            };



            Users = new HashSet<User>
            {
                 new User {Id = 127, CreatedOn = DateTime.Now, DisplayName = "Aleks Zarev", Username = "aleks1", Password = "1234", Email = "aleks@zarev.com", RoleId = 1},
            };

        }
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(Roles);

            modelBuilder.Entity<User>().HasData(Users);

            modelBuilder.Entity<Category>().HasData(Categories);

        }


    }
}