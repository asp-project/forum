﻿using Forum.Data.Models;
using Forum.MVC.Attributes;
using Forum.MVC.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Models;
using Forum.Service.Contracts;
using Forum.WebAPI.Models.ModelMapper;
using Forum.WebAPI.Models.WebModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Forum.MVC.Controllers
{
    public class PostController : Controller
    {
        private readonly ICategoryRepository categoryRepository;
        private readonly IPostRepository postRepository;
        private readonly IPostVoteRepository voteRepository;
        private readonly IUserRepository userRepository;
        private readonly ICommentRepository commentRepository;
        private readonly ICommentService commentService;
        private readonly IPostService postService;

        public PostController(ICategoryRepository categoryRepository,
                                   IPostRepository postRepository,
                                   IPostVoteRepository voteRepository,
                                   IUserRepository userRepository,
                                   ICommentRepository commentRepository,
                                   ICommentService commentService,
                                   IPostService postService)

        {
            this.categoryRepository = categoryRepository;
            this.postRepository = postRepository;
            this.voteRepository = voteRepository;
            this.userRepository = userRepository;
            this.commentRepository = commentRepository;
            this.commentService = commentService;
            this.postService = postService;
        }

        [MyAuthorization]
        public IActionResult Comment([FromQuery] string category)
        {
            var forum = new CommentWebModel();
            return PartialView(forum);
        }

        [MyAuthorization]
        public IActionResult SendComment(CommentWebModel commentWebModel, [FromQuery] int postId)
        {
            if (commentWebModel.Content == null)
            {
                return PartialView("Error", "Add content please.");
            }
            var comment = ModelMapper.ToModel(commentWebModel);
            comment.PostId = (int)this.HttpContext.Session.GetInt32("PostId");
            var userMail = this.HttpContext.Session.GetString("Email");
            var userId = this.userRepository.QueryAll()
                                            .FirstOrDefault(x => x.Email == userMail).Id;
            this.commentService.AddCommentToPost(comment, userId);
            return this.PartialView();

        }

        [MyAuthorization]
        public IActionResult Post([FromQuery] string category)
        {

            var model = new PostWebModel();
            var categoryId = this.categoryRepository.QueryAll()
                .Where(x => x.Title == category)
                .FirstOrDefault().Id;

            model.CategoryId = categoryId;
            return PartialView(model);

        }

        [MyAuthorization]
        public IActionResult Vote([FromQuery] int postId,
            [FromQuery] string type,
            [FromQuery] bool? comment,
            [FromQuery] int? page)

        {
            PostVote vote = new PostVote();
            vote.PostId = postId;

            var userMail = this.HttpContext.Session.GetString("Email");
            if (type == "up" && !this.userRepository.QueryAll()
                                                      .FirstOrDefault(x => x.Email == userMail)
                                                      .Votes.Any(x => x.PostId == postId && x.VoteType == Data.Enums.VoteType.Upvote))
            {
                vote.VoteType = Data.Enums.VoteType.Upvote;
            }
            else if (type == "down")
            {
                vote.VoteType = Data.Enums.VoteType.Downvote;
            }

            vote.UserId = this.userRepository.QueryAll()
                                                     .FirstOrDefault(x => x.Email == userMail).Id;
            this.voteRepository.Add(vote);

            var categoryId = this.postRepository.QueryAll()
                                                         .Where(x => x.Id == postId)
                                                         .FirstOrDefault().CategoryId;

            var categoryName = this.categoryRepository.QueryAll()
                                                                .Where(x => x.Id == categoryId)
                                                                .FirstOrDefault().Title;
            var model = ForumModel(categoryName, postId, comment, page);
            if (model.CommentVote == false)
            {
                return this.Redirect($"https://localhost:5001/categories/index?category={categoryName}&page={model.PageNumber}&comment=false");
            }
            else
            {
                return this.Redirect($"https://localhost:5001/categories/post?category={categoryName}&id={model.PostId}");
            }

        }
        [MyAuthorization]
        public IActionResult ErrorMessage(string message)
        {
            var errorModel = new ErrorViewModel();
            errorModel.Message = message;
            return this.PartialView(errorModel);
        }



        [MyAuthorization]
        public IActionResult SendPost(PostWebModel postWebModel, [FromQuery] int categoryId)
        {
            try
            {
                var post = ModelMapper.ToModel(postWebModel);
                post.Title = postWebModel.Title;
                post.Content = postWebModel.Content;
                post.CategoryId = categoryId;
                var userMail = this.HttpContext.Session.GetString("Email");
                var userId = this.userRepository.QueryAll().FirstOrDefault(x => x.Email == userMail).Id;
                this.postService.Create(post, userId);
                return this.PartialView();
            }
            catch
            {
                if (postWebModel.Content == null && postWebModel.Title == null)
                {
                    return PartialView("Error", "Empty fields");
                }
                else
                {
                    if (postWebModel.Content == null)
                    {
                        return PartialView("Error", "Add content please.");

                    }
                    else if (postWebModel.Title == null)
                    {
                        return PartialView("Error", "Add title please.");

                    }
                }
                return PartialView("Errore", "Invalid request");
            }
        }
        public ForumModel ForumModel([FromQuery] string category,
                                     [FromQuery] int id,
                                     [FromQuery] bool? comment,
                                     [FromQuery] int? page)

        {
            var order = new Order(true, "CreatedOn");
            var pagination = new Pagination(0, 3);
            var filter = new PostFilter();
            var categories = this.categoryRepository.QueryAll();
            var posts = this.postRepository.QueryAll()
                                        .Where(x => x.Category.Title == $"{category}")
                                        .Include(x => x.Comments);

            var votes = this.voteRepository.QueryAll();

            var comments = this.commentRepository.QueryAll();


            var forum = new ForumModel()
            {
                Comments = comments,
                Votes = votes,
                CategoryName = category,
                Categories = categories,
                Posts = posts,
                PostId = id,
                CommentVote = comment,
                PageNumber = page

            };

            return forum;
        }

        public IActionResult Error(Exception ex)
        {
            return PartialView(ex);
        }
    }
}
