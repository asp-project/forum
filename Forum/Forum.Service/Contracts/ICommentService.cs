﻿using Forum.Data.Models;
using Forum.Repository.Models;
using Forum.Service.DTOs;
using System.Linq;

namespace Forum.Service.Contracts
{
    public interface ICommentService : IService<Comment>
    {
        IQueryable<CommentDTO> GetAll(Pagination pagination, bool includeDeleted = false);

        IQueryable<CommentDTO> GetById(int id, bool includeDeleted = false);
        void AddCommentToPost(Comment comment, int userId);
        void DeleteAsUser(int id, int userId);
    }
}
