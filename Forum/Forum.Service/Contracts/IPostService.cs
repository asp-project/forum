﻿using Forum.Data.Models;
using Forum.Repository.Models;
using Forum.Service.DTOs;
using System.Linq;

namespace Forum.Service.Contracts
{
    public interface IPostService : IService<Post>
    {
        IQueryable<PostDTO> GetAll(Pagination pagination, Order order, PostFilter filter, bool includeDeleted = false);

        IQueryable<PostDTO> GetById(int id, bool includeDeleted = false);
        IQueryable<PostDTO> GetRecent();

        IQueryable<PostDTO> GetTopRated();

        void Create(Post post, int userId);

        void DeleteAsUser(int id, int userId);
    }
}
