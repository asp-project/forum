﻿using Forum.Data.Models;

namespace Forum.Service.DTOs
{
    public class CommentDTO
    {
        public CommentDTO()
        {

        }
        public CommentDTO(Comment comment)
        {
            Id = comment.Id;
            Content = comment.Content;
            PossitiveVoteCount = GetPossitiveVotes(comment);
            NegativeVoteCount = GetNegativeVotes(comment);
            AuthorName = comment.Author.IsDeleted ? "[Deleted user]" : comment.Author.DisplayName;
            PostId = comment.PostId;
        }
        public int Id { get; set; }
        public string Content { get; set; }
        public string AuthorName { get; set; }
        public int PossitiveVoteCount { get; set; }
        public int NegativeVoteCount { get; set; }
        public int PostId { get; set; }

        private int GetPossitiveVotes(Comment comment)
        {
            var votes = 0;
            foreach (var vote in comment.Votes)
            {
                if (vote.VoteType == Data.Enums.VoteType.Upvote && vote.IsDeleted == false) votes++;
            }
            return votes;
        }

        private int GetNegativeVotes(Comment comment)
        {
            var votes = 0;
            foreach (var vote in comment.Votes)
            {
                if (vote.VoteType == Data.Enums.VoteType.Downvote && vote.IsDeleted == false) votes++;
            }
            return votes;
        }
    }
}
