﻿using Forum.Data.Abstraction;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Forum.Data.Models
{
    public class Category : Entity
    {
        [Required]
        [StringLength(Constants.CATEGORY_TITLE_MAX_LEN)]
        public string Title { get; set; }

        public int AuthorId { get; set; }
        public User Author { get; set; }

        public ICollection<Post> Posts { get; set; } = new List<Post>();

    }
}
