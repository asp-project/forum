﻿
namespace Forum.Repository.Models
{
    public class PostFilter
    {
        public PostFilter(string name = default, string title = default, string content = default)
        {
            this.AuthorName = name;
            this.Title = title;
            this.Content = content;
        }
        public string AuthorName { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
