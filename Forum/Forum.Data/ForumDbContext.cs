﻿using Forum.Data.Contracts;
using Forum.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Forum.Data
{
    public class ForumDbContext : DbContext, IDatabase
    {
        public ForumDbContext()
        {

        }

        public ForumDbContext(DbContextOptions<ForumDbContext> options)
            : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PostVote> Votes { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Role> Roles { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            if (!builder.IsConfigured)
            {
                builder.UseSqlServer("Server=.;Database=Forum;Trusted_Connection=True;");
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            modelBuilder.Seed();
        }
        public override int SaveChanges()
        {
            //UpdateSoftDeleteStatuses();
            ApplyOnEntityCreate();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            //UpdateSoftDeleteStatuses();
            ApplyOnEntityCreate();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
        //ILoggerFactory
        private void ApplyOnEntityCreate()
        {
            var entries = this.ChangeTracker
                .Entries()
                .Where(e =>
                    e.Entity is IEntity && e.State == EntityState.Added);

            foreach (var entry in entries)
            {
                var entity = (IEntity)entry.Entity;
                if (entry.State == EntityState.Added || entity.CreatedOn == null)
                {
                    entity.CreatedOn = DateTime.Now;
                }
            }
        }

        //private void UpdateSoftDeleteStatuses()
        //{
        //    foreach (var entry in ChangeTracker.Entries())
        //    {
        //        switch (entry.State)
        //        {
        //            case EntityState.Added:
        //                entry.CurrentValues["IsDeleted"] = false;
        //                break;
        //            case EntityState.Deleted:
        //                entry.State = EntityState.Modified;
        //                entry.CurrentValues["IsDeleted"] = true;
        //                break;
        //        }
        //    }
        //}
    }
}
