﻿using Forum.Service.Contracts;
using Forum.WebAPI.Extension;
using Forum.WebAPI.Models;
using Forum.WebAPI.Models.ModelMapper;
using Forum.WebAPI.Models.WebModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Forum.WebAPI.Controllers.API
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryService categoryService;
        public CategoriesController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }


        // GET: api/[controller]
        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll(bool withDeleted, [FromQuery] PaginationModel paginationModel)
        {
            var pagination = ModelMapper.ToModel(paginationModel);
            var result = categoryService.GetAll(pagination, withDeleted);
            return Ok(result);

        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult Get(int id, bool withDeleted)
        {

            var result = categoryService.GetById(id, withDeleted);
            return Ok(result);

        }
        // POST: api/[controller]/category
        [Authorize(Roles = "Moderator, Admin")]
        [HttpPost("create")]
        public IActionResult Create([FromBody] CategoryWebModel model)
        {
            var userId = User.GetUserID();
            var category = ModelMapper.ToModel(model);
            category.AuthorId = userId;
            categoryService.Create(category);

            return Created("Posted", model);
        }

        [Authorize(Roles = "Moderator, Admin")]
        [HttpPost("update/{id}")]
        public IActionResult Update(int id, [FromBody] CategoryWebModel model)
        {
            var userId = User.GetUserID();
            var category = ModelMapper.ToModel(model);
            categoryService.Update(id, category, userId);

            return Ok();
        }

        [Authorize(Roles = "Moderator, Admin")]
        [HttpPost("restore/{id}")]
        public IActionResult Restore(int id)
        {
            categoryService.Restore(id);
            return Ok();
        }

        [Authorize(Roles = "Moderator, Admin")]
        [HttpDelete("delete/{id}")]
        public IActionResult Delete(int id)
        {
            categoryService.Delete(id);
            return NoContent();
        }
    }
}

