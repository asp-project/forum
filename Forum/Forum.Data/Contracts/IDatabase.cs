﻿using Forum.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum.Data.Contracts
{
    public interface IDatabase
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<PostVote> Votes { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Role> Roles { get; set; }

        public int SaveChanges();
    }
}
