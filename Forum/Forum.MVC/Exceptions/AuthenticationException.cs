﻿using System;

namespace Forum.MVC.Exceptions
{
    public class AuthenticationException : ApplicationException
    {
        public AuthenticationException()
        {
        }

        public AuthenticationException(string message)
            : base(message)
        {
        }
    }
}
