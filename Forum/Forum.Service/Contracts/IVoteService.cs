﻿using Forum.Data.Enums;

namespace Forum.Service.Contracts
{
    public interface IVoteService
    {
        VoteType GetUserVoteTypeForPost(int postId, int userId);
        VoteType GetUserVoteTypeForCommnet(int commentId, int userId);

        void TogglePostVote(int postId, int userId, VoteType newVoteType);
        void ToggleCommentVote(int commentId, int userId, VoteType newVoteType);
    }
}
