﻿using Microsoft.AspNetCore.Mvc;

namespace Forum.WebAPI.Models
{
    public class PaginationModel
    {
        [FromQuery(Name = "offset")]
        public int Offset { get; set; }
        [FromQuery(Name = "limit")]
        public int Limit { get; set; }
    }
}
