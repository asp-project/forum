﻿using Forum.Data.Abstraction;
using System.Collections.Generic;

namespace Forum.Data.Models
{
    public class Comment : Entity
    {
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public virtual User Author { get; set; }
        public int PostId { get; set; }
        public virtual Post Post { get; set; }

        public virtual ICollection<CommentVote> Votes { get; set; } = new List<CommentVote>();
    }
}
