﻿using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Models;
using Forum.Service.Exceptions;
using Forum.Service.Helpers;
using Forum.Service.Services;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Tests.ServiceTests
{
    [TestClass]
    public class UserServiceTest
    {
        private readonly UserService sut;
        private List<User> testList;

        private readonly PostFilter filter = new PostFilter();
        private Pagination pagination = new Pagination(0, int.MaxValue);

        private readonly Mock<IUserRepository> userRepository = new Mock<IUserRepository>();
        private readonly Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

        public UserServiceTest()
        {
            sut = new UserService(this.userRepository.Object, appSettings.Object);
            testList = new List<User>()
            {
              new User
              {
                  Id = 2,
                  CreatedOn = DateTime.Now,
                  DisplayName = "Aleks Zarev",
                  Email = "my@email.com",
                  Username = "aleks1",
                  Password = "1234",
                  Role = new Role()
                  {
                      Id=1,
                      RoleName = "Admin"
                  },
                  RoleId=1,
                  ProfileUrl = "test",

              },
              new User
              {
                  Id = 3,
                  CreatedOn = DateTime.Now,
                  DisplayName = "Aleks Zarev 2",
                  Email = "my@email2.com",
                  Username = "aleks2",
                  Password = "1234",
                  Role = new Role()
                  {
                      Id=2,
                      RoleName = "Moderator"
                  },
                  RoleId=2,
                  ProfileUrl = "test2",
              }

            };

        }

        [TestMethod]
        public void UsersShould_ReturnAll()
        {
            //Arrange           
            userRepository.Setup(x => x.QueryAll(this.pagination, null)).Returns(testList.AsQueryable());

            //Act
            var result = sut.GetAll(this.pagination, null);

            //Assert
            Assert.AreEqual(testList.Count, result.ToList().Count());

        }


        [TestMethod]
        public void UsersShould_ThrowExceptionWhen_UsersNotAdded()
        {
            //Arrange
            testList = new List<User>();
            userRepository.Setup(x => x.QueryAll(this.pagination, null)).Returns(testList.AsQueryable());

            //Act & Assert
            var e = Assert.ThrowsException<EntityNotFoundException>(() => sut.GetAll(this.pagination, null));
            Assert.AreSame(e.Message, "There are no users!");

        }



    }
}
