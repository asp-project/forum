﻿using Forum.Data.Models;
using Forum.Repository.Contacts;
using Forum.Repository.Models;
using Forum.Service.Exceptions;
using Forum.Service.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Tests.ServiceTests
{
    [TestClass]
    public class CategoryServiceTest
    {
        private readonly CategoryService sut;
        private List<Category> testList;

        private Pagination pagination = new Pagination(0, int.MaxValue);

        private readonly Mock<ICategoryRepository> categoryRepository = new Mock<ICategoryRepository>();
        private readonly Mock<IPostRepository> postRepository = new Mock<IPostRepository>();

        public CategoryServiceTest()
        {
            sut = new CategoryService(this.categoryRepository.Object, this.postRepository.Object);
            testList = new List<Category>()
            {
              new Category
              {
                  Id = 2,
                  AuthorId = 1,
                  Author = new User()
                  {
                      Username = "Kiril"
                  },
                  CreatedOn = DateTime.Now,
                  Title = "test"
              },
              new Category
              {
                  AuthorId = 3,
                  Author = new User()
                  {
                      Username = "Alejandro"
                  },
                  CreatedOn = DateTime.Now,
                  Id = 4,
                  Title = "test"
              }

            };

        }

        [TestMethod]
        public void CategoriesShould_ReturnAllCategories_WithoutDeleted()
        {
            //Arrange           
            var includeDeleted = false;
            categoryRepository.Setup(x => x.QueryAll(this.pagination)).Returns(testList.AsQueryable());

            //Act
            var result = sut.GetAll(this.pagination, includeDeleted);

            //Assert
            Assert.AreEqual(testList.Count, result.ToList().Count());

        }


        [TestMethod]
        public void CategoriesShould_ThrowExceptionWhen_CategoriesNotAdded()
        {
            //Arrange
            testList = new List<Category>();
            categoryRepository.Setup(x => x.QueryAll(this.pagination)).Returns(testList.AsQueryable());

            //Act & Assert
            var e = Assert.ThrowsException<EntityNotFoundException>(() => sut.GetAll(this.pagination));
            Assert.AreSame(e.Message, "There are no categories yet.");

        }

        [TestMethod]
        public void CategoriesShould_ReturnAllCategories_IncludeDeleted()
        {
            //Arrange
            var includeDeleted = true;
            categoryRepository.Setup(x => x.QueryAllWithDeleted(this.pagination)).Returns(testList.AsQueryable());

            //Act
            var result = sut.GetAll(this.pagination, includeDeleted);

            //Assert
            Assert.AreEqual(testList.Count, result.ToList().Count());

        }

        [TestMethod]
        public void CategoriesShould_ThrowExceptionWhen_CategoriesNotAdded_IncludeDeleted()
        {
            //Arrange
            var includeDeleted = true;
            testList = new List<Category>();
            categoryRepository.Setup(x => x.QueryAllWithDeleted(this.pagination)).Returns(testList.AsQueryable());

            //Act & Assert
            var e = Assert.ThrowsException<EntityNotFoundException>(() => sut.GetAll(this.pagination, includeDeleted));
            Assert.AreSame(e.Message, "There are no categories yet.");

        }

        [Ignore]
        [TestMethod]
        public void CategoriesShould_ReturnCorrectObject_WhenMethodGetById_IsCalled()
        {
            //Arrange
            categoryRepository.Setup(x => x.QueryAll(this.pagination)).Returns(testList.AsQueryable());
            //Act

            var result = sut.GetById(2);
            //Assert
            Assert.AreEqual(testList[0].Title, result.Title);

        }


    }

}
